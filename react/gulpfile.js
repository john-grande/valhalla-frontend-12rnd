const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');
const run = require('gulp-run');

gulp.task('build-css', () => {
  return gulp.src('public/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/sass/'));
});

gulp.task('clean-css', () => {
  return del(['public/sass/app.css']);
});

gulp.task('default', gulp.series(['clean-css', 'build-css']));
