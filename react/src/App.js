import React, { Component } from 'react';
import Header from './layout/Header';
import HeroBanner from './layout/HeroBanner';
import FeatureBoxes from './layout/FeatureBoxes';
import OfferSection from './layout/OfferSection';
import NumbersSection from './layout/NumbersSection';
import KeepThingsSimple from './layout/KeepThingsSimple';
import FranchiseeSection from './layout/FranchiseeSection';
import RequestQuoteSection from './layout/RequestQuoteSection';
import FeaturedBoxSection from './layout/FeaturedBoxSection';
import FranchiseesSay from './layout/FranchiseesSay';
import ImgGallerySection from './layout/ImgGallerySection';
import FooterSection from './layout/FooterSection';

class App extends Component {
  render() {
    return(
      <div className="app-main">
        <Header />
        <main>
          <HeroBanner />
          <FeatureBoxes />
          <OfferSection />
          <NumbersSection />
          <KeepThingsSimple />
          <FranchiseeSection />
          <RequestQuoteSection />
          <FeaturedBoxSection />
          <FranchiseesSay />
          <ImgGallerySection />
        </main>
        <FooterSection />
      </div>
    );
  }
}

export default App;
