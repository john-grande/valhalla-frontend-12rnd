import React, { Component } from 'react';

class FooterSection extends Component{
  render(){
    return(
      <footer>
        <div className="flex">
          <a className="logo" href="#">
            <span>12RND FITNESS</span>
          </a>
          <nav className="bottom-nav">
            <a href="#">The Experience</a>
            <a href="#">The Training Camp</a>
            <a href="#">Train At Home</a>
            <a href="#">12RND Blog</a>
            <a href="#">Press</a>
            <a href="#">Privacy &amp; GDPR</a>
            <a href="#">Terms &amp; Conditions</a>
            <a href="#">FAQs</a>
            <a href="#">Contact Us</a>
            <a href="#">Become a Franchisee</a>
          </nav>
          <div className="soc-links-box">
            <h5>Follow 12RND Online</h5>
            <ul className="soc-links">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">YouTube</a></li>
              <li><a href="#">Instagram</a></li>
              <li><a href="#">Spotify</a></li>
            </ul>
            <span className="footer-text">Copyright © 2020, 12RND Fitness All Rights Reserved.</span>
            <span className="footer-text">Other marks belong to their respective owners.</span>
          </div>
        </div>
      </footer>
    );
  };
};

export default FooterSection;
