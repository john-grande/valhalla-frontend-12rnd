import React, { Component } from 'react';

class FeaturedBoxSection extends Component{
  render(){
    return(
      <section class="featured-box">
        <div class="bounding-box">
          <h3>FEATURED IN</h3>
          <ul class="flex featured-links">
            <li class="relauncher">
              <a href="https://www.relauncher.com.au/blog/how-tim-west-went-from-a-personal-trainer-to-create-his-own-franchise-brand" target="_blank">
                <span class="title">HOW TIM WEST WENT FROM A PERSONAL TRAINER TO CREATE HIS OWN FRANCHISE BRAND</span>
                <span class="company">Relauncher</span>
                <span class="date"><span class="day">10</span> Feb 2020</span>
              </a>
            </li>
            <li class="insider-business">
              <a href="https://insidesmallbusiness.com.au/featured/translating-a-unique-concept-into-a-thriving-business" target="_blank">
                <span class="title">TRANSLATING A UNIQUE CONCEPT INTO A THRIVING BUSINESS INSIDE SMALL BUSINESS</span>
                <span class="company">Insider Business</span>
                <span class="date"><span class="day">14</span> Oct 2020</span>
              </a>
            </li>
            <li class="smart-company">
              <a href="https://www.smartcompany.com.au/business-advice/franchising/12rnd-fitness-growth-covid-19/" target="_blank">
                <span class="title">How 12RND Fitness withstood the closure of 80 franchises during COVID-19</span>
                <span class="company">Smart Company</span>
                <span class="date"><span class="day">4</span> Jun 2020</span>
              </a>
            </li>
          </ul>
        </div>
      </section>
    );
  };
};

export default FeaturedBoxSection;
