import React, { Component } from 'react';

class NumbersSection extends Component {
  render(){
    return(
      <section className="numbers-box">
        <div className="bounding-box">
          <figure className="video">
            <object data='https://www.youtube.com/embed/eg4H6u76dEI'></object>
          </figure>
          <ul className="numbers-box">
            <li>
              <span className="title">3</span>
              <span>Countries</span>
            </li>
            <li>
              <span className="title">0+</span>
              <span>Clubs Open</span>
            </li>
            <li>
              <span className="title">100+</span>
              <span>Territories sold</span>
            </li>
            <li>
              <span className="title">14,000+</span>
              <span>Members</span>
            </li>
          </ul>
        </div>
      </section>
    );
  };
};

export default NumbersSection;
