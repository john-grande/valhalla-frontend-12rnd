import React, { Component } from 'react';

class KeepThingsSimple extends Component {
  render(){
    return(
      <section className="keep-things-simple">
        <div className="bounding-box">
          <article>
            <h3>Our product differentiators</h3>
            <p>WE BELIEVE IN KEEPING THINGS SIMPLE.</p>
            <p>We focus on Convenience + Enjoyment + Results to provide an unparalleled experience for members.</p>
          </article>
          <ul className="content-boxes">
            <li className="feature-box-1">
              <h4>Convenience</h4>
              <p>Convenience is our highest priority, as we understand the importance of usage and how this influences length of stay. We have overcome the biggest source of complaints in group fitness; className times and bookings. Our focus on convenience has allowed us to remove barriers of entry for fitness, both at the club and away from it.</p>
            </li>
            <li className="feature-box-2">
              <h4>Enjoyment</h4>
              <p>Exercise is commonly used as penance. We flip this mindset on its head, and by including coaches, skill development and gamification we bring back the joy people derive from individual and team sports. This creates the intrinsic motivation that keeps members coming back for more.</p>
            </li>
            <li className="feature-box-3">
              <h4>Results</h4>
              <p>As our model is easier to attend and more enjoyable than our counterparts, we are already on the way to guaranteeing our members life changing results. To build upon this, with the inclusion of sports science based programming and cutting edge technology, we can confidently say that members at 12RND Fitness clubs will get better results than they ever have before.</p>
            </li>
          </ul>
        </div>
      </section>
    );
  };
};

export default KeepThingsSimple;
