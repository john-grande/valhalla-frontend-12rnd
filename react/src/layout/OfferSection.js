import React, { Component } from 'react';

class OfferSection extends Component {
  render(){
    return(
      <section className="offer-box">
        <div className="bounding-box">
          <article className="what-we-offer">
            <div className="flex">
              <h2>WE ARE THE FASTEST GROWING BOXING & STRENGTH PROVIDER IN THE WORLD</h2>
              <p>Co-founded by Managing Director, Tim West and four-time world champion Australian boxer, Danny Green, 12RND Fitness began franchising in 2016. In 2019, the brand expanded overseas under the name UBX Training, with the first international club opening in New Zealand. As of 2020, there are over 80 locations across Australia, New Zealand and Singapore, and due to open in the US and UK within the year</p>
            </div>
            <div className="background-img"></div>
          </article>
        </div>
      </section>
    );
  };
};

export default OfferSection;
