import React, { Component } from 'react';

class FranchiseeSection extends Component{
  render(){
    return(
      <section className="franchisee-box">
        <div className="bounding-box">
          <article>
            <h3>YOUR JOURNEY AS A FRANCHISEE</h3>
            <p>At every stage of your business journey, you will be supported by a team of experts who have years of combined experience in all of the key areas required for business success, such as fitness, sales, marketing, operations and technology.</p>
          </article>
          <ul className="enumerated-boxes">
            <li>
              <h4>Onboarding</h4>
              <ul className="enumerated-box">
                <li><span>Real Estate Guidance for site selection</span></li>
                <li><span>One on one business development and training</span></li>
                <li><span>Practical and business workshop at 12RND HQ</span></li>
                <li><span>Finance Solutions with Pre approved options</span></li>
              </ul>
            </li>
            <li>
              <h4>Launch</h4>
              <ul className="enumerated-box">
                <li><span>Easy to manage design and fit out system</span></li>
                <li><span>Guided, tried and tested 12 week pre-sell marketing strategy</span></li>
                <li><span>Simple and effective sale process</span></li>
                <li><span>One on one pre-sale coaching and support</span></li>
                <li><span>Personalised project management system</span></li>
                <li><span>A formula to achieve pre-rent break even prior to opening</span></li>
              </ul>
            </li>
            <li>
              <h4>Operation</h4>
              <ul className="enumerated-box">
                <li><span>Connect with the purposely built 12RND operational systems</span></li>
                <li><span>Digital and local area marketing strategy & Content</span></li>
                <li><span>National driven PR and brand awareness</span></li>
                <li><span>Simple to execute retention strategies</span></li>
                <li><span>PwC’s financial management solution</span></li>
                <li><span>Ongoing 1 on 1 and team support</span></li>
              </ul>
            </li>
          </ul>
        </div>
      </section>
    );
  };
};

export default FranchiseeSection;
