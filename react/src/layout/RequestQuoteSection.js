import React, { Component } from 'react';

class RequestQuoteSection extends Component{
  render(){
    return(
      <section className="request-quote">
        <div className="bounding-box">
          <div className="request-quote-box">
            <h4>Contact us today</h4>
            <p>For more information and to speak with one of the team, please complete the below details and we will be in touch soon.</p>
            <form method="post">
              <div className="flex-form">
                <div className="input">
                  <input name="first-name" required type="text" placeholder="First Name"/>
                </div>
                <div className="input">
                  <input name="last-name" required type="text" placeholder="Last Name"/>
                </div>
                <div className="input">
                  <input name="email" required type="email" placeholder="youremail@provider.com"/>
                </div>
                <div className="input">
                  <input name="phone-name" required type="number" placeholder="###-####"/>
                </div>
                <div className="dropdown">
                  <label for="interest">What best defines your interest in becoming a franchisee <span className="important">*</span></label>
                  <select name="interest" required>
                    <option name="default" selected disabled>-- Please select --</option>
                    <option>Looking to transition out of current employment</option>
                    <option>Current Fitness business owner looking to diversify or change sector</option>
                    <option>Current Investor looking for opportunities</option>
                    <option>Fitness professional looking to progress and set up my own business</option>
                    <option>Other (Please specify in Comments)</option>
                  </select>
                </div>
                <div className="input full-size">
                  <label for="country-area">What's the Country and Area you would you like to open a club in? <span className="important">*</span></label>
                  <input name="country-area" required type="text" placeholder="Area and Country"/>
                </div>
                <div className="dropdown">
                  <label for="interest">What's your available capital <span className="important">*</span></label>
                  <select name="interest" required>
                    <option name="default" selected disabled>-- Please select --</option>
                    <option>Below $100k</option>
                    <option>$100k - $199k</option>
                    <option>$200k +</option>
                  </select>
                </div>
                <div className="input">
                  <textarea name="supporting-comments" placeholder="Supporting Comments"></textarea>
                </div>
                <div className="button">
                  <button type="submit" name="request-quote">Request Quote</button>
                </div>
               </div>
            </form>
          </div>
        </div>
      </section>
    );
  };
};

export default RequestQuoteSection;
