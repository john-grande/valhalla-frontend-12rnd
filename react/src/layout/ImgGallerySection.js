import React, { Component } from 'react';

class ImgGallerySection extends Component{
  render(){
    return(
      <section className="img-gallery">
        <ul className="flex">
          <li className="img-a"><span></span></li>
          <li className="img-b"><span></span></li>
          <li className="img-c"><span></span></li>
          <li className="img-d"><span></span></li>
          <li className="img-e"><span></span></li>
        </ul>
      </section>
    );
  };
};

export default ImgGallerySection;
