import React, { Component } from 'react';

class FeatureBoxes extends Component {
  render(){
    return(
      <ul className="feature-boxes">
        <li className="box">
          <span className="title">Low Start Up Capital</span>
          <span className="description">$200,000 - $220,000 with pre-approved ﬁnance options available</span>
        </li>
        <li className="box">
          <span className="title">Unique Proven Concept</span>
          <span className="description">No Fixed className Times + Boxing + Strength Training</span>
        </li>
        <li className="box">
          <span className="title">A Team of Experts</span>
          <span className="description">Complete strategic support including marketing and operations.</span>
        </li>
        <li className="box">
          <span className="title">Highly Profitable</span>
          <span className="description">Small footprint + high member capacity = low breakeven + a high yielding opportunity</span>
        </li>
        <li className="box">
          <span className="title">No Fitness Experience Required</span>
          <span className="description">We welcome entrepreneurs of all backgrounds</span>
        </li>
      </ul>
    );
  };
};

export default FeatureBoxes;
