import React, { Component } from 'react';

class Header extends Component {
  render(){
    return (
      <header>
        <section className="top-header">
          <a className="logo" href="#">
            <span>12RND FITNESS</span>
          </a>
          <div className="blurb">
            BECOME A 12RND FRANCHISEE
          </div>
          <div className="buttons">
            <a href="#">CONTACT US TODAY</a>
          </div>
        </section>
        <section className="top-navigation">
          <nav>
            <a href="#">FREE TRIAL</a>
            <a href="#">FIND A CLUB</a>
            <a href="#">TRAIN AT HOME</a>
            <a href="#">THE TRAINING CAMP</a>
            <a href="#">THE EXPERIENCE</a>
            <a href="#">OWN A 12RND</a>
          </nav>
        </section>
      </header>
    );
  }
}
export default Header;
