import React, { Component } from 'react';

class HeroBanner extends Component{
  render(){
    return(
      <section className="hero-banner">
        <div className="bg-overlay"></div>
        <div className="bounding-box">
          <div className="title">BECOME A 12RND FRANCHISEE</div>
          <h1 className="description">The most dynamic, convenient and effective workout, supported by a simple, high returning business model; a formula for growth.</h1>
        </div>
      </section>
    );
  };
};

export default HeroBanner;
