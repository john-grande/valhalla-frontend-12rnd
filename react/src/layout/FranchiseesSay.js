import React, { Component } from 'react';

class FranchiseesSay extends Component{
  render(){
    return(
      <section className="what-franchisees-say">
        <div className="bounding-box">
          <h3>What our Franchisees have to say</h3>
          <div className="slideshow-box">
            <ul className="flex slideshow">
              <li>
                <img src="img/12RND Fitness_Julian Lallo-48.jpg" />
                <div className="context">
                  <p>“Outside of offering an amazing training experience with the combination of boxing and functional strength, it is personable, and it is like PT on steroids” </p>
                  <span className="author">- Boyd and Taylor, 12RND South Yarra Franchisees</span>
                </div>
              </li>
              <li>
                <img src="img/12RND Fitness_Julian Lallo-48.jpg" />
                <div className="context">
                  <p>“I do really love the product, it’s easy to promote and I like seeing the members get results. I love the people that I work with and the feeling of being able to grow a successful business that helps people change their lives!”</p>
                  <span className="author">- Nicky - 12RND Labrador Club Manager</span>
                </div>
              </li>
              <li>
                <img src="img/12RND Fitness_Julian Lallo-48.jpg" />
                <div className="context">
                  <p>“The biggest stand out point on deciding on 12RND for us was its strong marketing presence on social media. We were looking for a fitness concept that was community-based, unique, new and different to anything else! “</p>
                  <span className="author">- Steve and Allison - 12RND Bella Vista Franchisees</span>
                </div>
              </li>
            </ul>
          </div>
          <div className="slideshow-controls">
            <ul className="flex">
              <li><span></span></li>
              <li><span></span></li>
              <li><span></span></li>
            </ul>
          </div>
        </div>
      </section>
    );
  };
};

export default FranchiseesSay;
